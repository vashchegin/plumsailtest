﻿using NLog;
using PlumsailTest.DB.Context;
using PlumsailTest.DB.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PlumsailTest.API.Controllers.API
{
    [RoutePrefix("api/contact")]
    [EnableCors(origins: "http://localhost:53367", headers: "*", methods: "*")]
    public class ContactController : ApiController
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        private Repository<Contact> contactRepository;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public ContactController()
        {
            contactRepository = unitOfWork.Repository<Contact>();
        }

        /// <summary>
        /// Get all contact from database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]        
        public IHttpActionResult GetAll()
        {
            logger.Info("Get all contacts");
            List<Contact> result = new List<Contact>();
            result.AddRange(contactRepository.Table.ToList());
            return Ok(result);
        }
        
        /// <summary>
        /// Get specified contact by ID
        /// </summary>
        /// <param name="id">ID/Key of contact in DB</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetById/{id:guid}")]
        public IHttpActionResult GetById(Guid id)
        {
            Contact toReturn = contactRepository.GetById(id);
            if (toReturn != null)
            {
                logger.Info("Get get contact with id = {0}", id);
                return Ok(toReturn);
            }
            else
            {
                logger.Error("Not found contact with id = {0}", id);
                return NotFound();
            }
        }

        /// <summary>
        /// Add a new contact to Database
        /// </summary>
        /// <param name="value">contact model</param>
        /// <returns></returns>
        [HttpPost]
        [Route("Add")]
        public IHttpActionResult AddContact([FromBody]Contact value)
        {
            if (value == null)
            {
                return BadRequest();
            }
            try
            {                
                contactRepository.Insert(value);
                logger.Info("Add a new contact = ({0})", value.ToString());
                return Ok(value.ID);
            }
            catch (Exception ex)
            {
                logger.Error("Error add a new contact = ({0})", value.ToString());
                return BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// Update existing contact by ID
        /// </summary>
        /// <param name="id">Contact ID</param>
        /// <param name="value">New contact value</param>
        /// <returns></returns>
        [HttpPut]
        [Route("Update/{id:guid}")]
        public IHttpActionResult UpdateContact(Guid id, [FromBody]Contact value)
        {
            Contact toUpdate = contactRepository.GetById(id);
            if (toUpdate != null)
            {
                toUpdate.BirthDay = value.BirthDay;
                toUpdate.Gender = value.Gender;
                toUpdate.Name = value.Name;
                toUpdate.NativeLanguage = value.NativeLanguage;
                toUpdate.SoccerFan = value.SoccerFan;
                try
                {                    
                    contactRepository.Update(toUpdate);
                    logger.Info("Update the contact with Id = {0}, with new content ({1})", id, value.ToString());
                    return Ok();
                }
                catch (Exception ex)
                {
                    logger.Error("Error update the contact with Id = {0} and new content = ({1})", id, value.ToString());
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                logger.Error("Not found contact for update with id = {0}", id);
                return NotFound();
            }
        }

        /// <summary>
        /// Delete existing contact by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("Remove/{id:guid}")]
        public IHttpActionResult DeleteContact(Guid id)
        {
            Contact cntToDelete = contactRepository.GetById(id);
            if (cntToDelete != null)
            {
                try
                {                    
                    contactRepository.Delete(cntToDelete);
                    logger.Info("Delete contact with id = {0}", id);
                    return Ok();
                }
                catch (Exception ex)
                {
                    logger.Error("Error delete the contact with Id = {0}", id);
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                logger.Error("Not found contact for delete with id = {0}", id);
                return NotFound();
            }
        }
    }
}
