﻿var moment = require('moment')
module.exports = function (value) {
  if (value) {
    return moment(String(value)).format('MM/DD/YYYY')
  }
}
