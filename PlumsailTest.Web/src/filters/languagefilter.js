﻿module.exports = function (value) {
  if (value === 0) {
    return 'English'
  } else if (value === 1) {
    return 'Russian'
  } else {
    return 'Deutsch'
  }
}
