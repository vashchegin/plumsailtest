import Vue from 'vue'
import Router from 'vue-router'
import AllContacts from '@/components/AllContacts'
import AddContact from '@/components/AddContact'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'AllContacts',
      component: AllContacts
    },
    {
      path: '/Add',
      name: 'AddContact',
      component: AddContact
    }
  ]
})
