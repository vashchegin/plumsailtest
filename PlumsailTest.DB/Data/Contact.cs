﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlumsailTest.DB.Data
{
    public enum Language
    {
        English,
        Russian,
        Deutsch
    }

    public enum Gender
    {
        Male,
        Female
    }

    public class Contact : BaseEntity
    {
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        public DateTime BirthDay { get; set; }

        [Required]
        public Language NativeLanguage { get; set; }

        [Required]
        public Gender Gender { get; set; }

        public bool SoccerFan { get; set; }

    }
}
