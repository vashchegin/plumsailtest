﻿To create new migration:
Add-Migration  -Name "<migration name>" -ProjectName "PlumsailTest.DB" -ConfigurationTypeName "PlumsailContextConfiguration" -StartupProject "PlumsailTest.API"

to update current db:
Update-Database -ProjectName "PlumsailTest.DB" -ConfigurationTypeName "PlumsailContextConfiguration" -StartupProject "PlumsailTest.API"