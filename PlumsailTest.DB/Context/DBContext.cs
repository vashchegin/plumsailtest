﻿using PlumsailTest.DB.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlumsailTest.DB.Context
{
    public class DBContext : DbContext
    {

        public DBContext() : base("PlumsailContext")
        {
        }

        public DbSet<Contact> Contacts { get; set; }

        public override int SaveChanges()
        {
            var selectedEntityList = ChangeTracker.Entries()
                                    .Where(x => x.Entity is BaseEntity &&
                                    (x.State == EntityState.Added || x.State == EntityState.Modified));

            foreach (var entity in selectedEntityList)
            {
                if (entity.State == EntityState.Added)
                {
                    ((BaseEntity)entity.Entity).AddedDate = DateTime.Now;
                    ((BaseEntity)entity.Entity).ModifiedDate = DateTime.Now;
                }
                if (entity.State == EntityState.Modified)
                {
                    ((BaseEntity)entity.Entity).ModifiedDate = DateTime.Now;
                }
            }
            return base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
