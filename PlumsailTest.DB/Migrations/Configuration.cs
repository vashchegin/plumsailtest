namespace PlumsailTest.DB.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class PlumsailContextConfiguration : DbMigrationsConfiguration<PlumsailTest.DB.Context.DBContext>
    {
        public PlumsailContextConfiguration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(PlumsailTest.DB.Context.DBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
