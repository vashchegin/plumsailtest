﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlumsailTest.API.Controllers.API;
using PlumsailTest.DB.Context;
using PlumsailTest.DB.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http.Results;

namespace PlumsailTest.API.Tests.Controllers
{
    [TestClass]
    public class ContactControllerTest
    {
        [AssemblyInitialize]
        public static void AssemblyInitialize(TestContext context)
        {
            Database.SetInitializer(new DBInitializer());
        }

        [TestInitialize]
        public void TestInitialize()
        {
            DBContext context = new DBContext();
            context.Database.Initialize(true);            
        }

        [TestMethod]
        public void GetAllContactsTest()
        {
            // Arrange
            ContactController controller = new ContactController();

            // Act
            var result = controller.GetAll();
            var contactsResponse = result as OkNegotiatedContentResult<List<Contact>>;

            // Asserts
            Assert.IsNotNull(contactsResponse.Content);
        }

        [TestMethod]
        public void GetByIdTest()
        {
            // Arrange
            ContactController controller = new ContactController();

            Contact newContact = new Contact()
            {
                Gender = Gender.Female,
                Name = "Test",
                NativeLanguage = Language.Russian,
                SoccerFan = true,
                BirthDay = DateTime.Today
            };
            // Act
            var result = controller.AddContact(newContact);
            var contactId = result as OkNegotiatedContentResult<Guid>;
            result = controller.GetById(contactId.Content);
            var contactOne = result as OkNegotiatedContentResult<Contact>;

            // Assert
            Assert.IsNotNull(contactOne.Content);
        }

        [TestMethod]
        public void AddContactTest()
        {
            // Arrange
            ContactController controller = new ContactController();

            Contact newContact = new Contact()
            {
                Gender = Gender.Male,
                Name = "Test 2",
                NativeLanguage = Language.English,
                SoccerFan = true,
                BirthDay = DateTime.Today
            };
            // Act
            var result = controller.AddContact(newContact);

            var contactId = result as OkNegotiatedContentResult<Guid>;

            // Assert
            Assert.IsNotNull(contactId);
            Assert.AreNotEqual(Guid.Empty, contactId.Content);
            
        }
        
        [TestMethod]
        public void UpdateContactTest()
        {
            ContactController controller = new ContactController();

            Contact newContact = new Contact()
            {
                Gender = Gender.Female,
                Name = "Test 3",
                NativeLanguage = Language.Deutsch,
                SoccerFan = true,
                BirthDay = DateTime.Today
            };
            // Act
            var result = controller.AddContact(newContact);
            var contactId = result as OkNegotiatedContentResult<Guid>;
            newContact.Gender = Gender.Male;
            result = controller.UpdateContact(contactId.Content, newContact);

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Delete()
        {
            // Arrange
            ContactController controller = new ContactController();

            Contact newContact = new Contact()
            {
                Gender = Gender.Male,
                Name = "Test 4",
                NativeLanguage = Language.Deutsch,
                SoccerFan = false,
                BirthDay = DateTime.Today
            };
            // Act
            var result = controller.AddContact(newContact);
            var contactId = result as OkNegotiatedContentResult<Guid>;
            result = controller.DeleteContact(contactId.Content);

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
