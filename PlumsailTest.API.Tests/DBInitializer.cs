﻿using PlumsailTest.DB.Context;
using System.Data.Entity;

namespace PlumsailTest.API.Tests
{
    public class DBInitializer : DropCreateDatabaseAlways<DBContext>
    {
        protected override void Seed(DBContext context)
        {
            context.SaveChanges();
        }
    }
}
